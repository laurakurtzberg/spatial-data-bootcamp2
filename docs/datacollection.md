Get all the Data!
=================
So, we need to have data. Where can you get data? Federal level government agencies are great resources for open data, and you can also find data projects on Github. It is best to stay away from state or local-level sources, since they may have errors. In some cases you may collect the data yourself.

Credible Data Sources
---------------------
If you need administrative boundaries (especially for international projects), you need [GADM](http://gadm.org/). They provide vector data in shapefile format for country borders and sub-divisons (provinces, for example).

For both raster and vector data, [NASA](http://open.nasa.gov/opendata) has a wealth of open data sources, including the Earth Observing System Data and Information System [(EOSDIS)](https://earthdata.nasa.gov/).

For raster data, including images from the famous LANDSAT satellites, USGS is a great data source. For elevation data, the USGS Digital elevation Models (DEM) are recommended. USGS data is freely available through their [Earth Explorer](http://earthexplorer.usgs.gov/) site.

If your project has to do with weather and climate, you will probably find data at [NOAA](http://www.ncdc.noaa.gov/data-access). They have data from satellites and from land-based data collection.

For vector data about the United States, your best resources are [TIGER](http://www.census.gov/geo/maps-data/data/tiger.html) products. They include boundaries, roads, and other information. They also have deomgraphic and economic data from the US Census (like population data!).

[Github](https://github.com/) is a place where a lot of open projects live, and it is common to find open data there as well. I recommend the Open Knowledge Foundations' [interface](http://data.okfn.org/data?q=spatial) to find quality spatial data on Github, especially for web map formats, like these [GeoJson Polygons](http://data.okfn.org/data/datasets/geo-boundaries-us-110m) for US states.

Speaking of web maps, [OpenStreetMap](http://www.openstreetmap.org) can be an excellent data source for streets, boundaries, and features all around the world. To use sources like OpenStreetMap, read the next sections about using web services.

We will get to data sources for style purposes [later](visualization.md).

Using Web Services
------------------
When you get or send data to a website, you use the HTTP protocol. Even just to visit a website, you specify this protocol in the URL - http:// etc. The HTTP protocol also includes methods like POST and GET.

Web Services work with the HTTP protocol to get or send spatial data to a web server.

Lets say you want to use information from Bing or OpenStreetMap to create your own map. You can access their spatial databases using Web Services - namely a Web Map Service (WMS) or a Web Feature Service (WFS).

Web Map Service allows you to work with map images in formats like JPEG or TIFF, and the Web Feature Service gives you features themselves (points, lines, and polygons) in a database or shapefile.

Web Map Service = a picture of the data
Web Feature Service = the data itself

Just like you use a URL to get or send data to a website, you use a URL to access spatial databases with WMS or WFS. This URL is more useful when you put it into a platform that supports Web Services, for example the open-source desktop GIS [QGIS](qgis.org) or the open-source software server [Geoserver](http://geoserver.org/). But it works in your browser, too.

Try clicking this one: [Mars: GetMap](http://www.mapaplanet.org/explorer-bin/imageMaker.cgi?map=Mars&VERSION=1.1.1&REQUEST=GetMap&SRS=IAU2000:49911&BBOX=45,-21,110,9&WIDTH=792&HEIGHT=365&LAYERS=mars_viking_merged&FORMAT=image/jpg)

This gets you a picture of some awesome spatial data - of planet Mars!
The URL you see in the browser is formatted in a specific way to get you the right picture.

Before we finish this section, I should mention another protocol called [OpeNDAP](http://www.opendap.org). It is useful for earth and climate scientists because it is used to work with data in NetCDF format. OpeNDAP also has their own software to read data from their servers. Government agencies like NASA and NOAA use OpeNDAP to share earth science data.


Now lets learn how to be a data janitor.
