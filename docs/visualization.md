Visualization of Spatial Data
=============================

Spatial data is generally represented by points, lines, or polygons on a map, but there are also 3D maps and models that go beyond those representations.

QGIS is your friend
-------------------

What is MapBox
--------------

Interaction and Animation
-------------------------
   Javascript can help your web maps become fancy and interactive.

Inspiration
-----------
   These maps show off the power of everything we have learned.
