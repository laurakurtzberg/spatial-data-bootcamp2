Scripting - Unleash the Code
============================
This is where it gets really good. Imagine this situation:
every week you get new data from a web service and add it
  to a spatial database to update your map. But it is always
  in one projection and you have to change it to another.
  Instead of typing requests to the web service, then spatial queries,
  and then GDAL commands every time ...
  you can put it all in one script and run it automatically every week.

WHAT DID I JUST SAY?
Thats right.
You do the work once, you do it well, and you will have a lovely workflow
forever.

APIs
----
