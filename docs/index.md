Student's Guide to Spatial Data and Mapping
===========================================

Introduction
------------
Map-making is fascinating as an example of a practice that stretches from ancient art to modern cutting-edge technology. In the current information age, we have this new idea: data on a map is powerful. Mapping data brings it closer to the real world and to our sense of place.

So, we need to know how to use spatial data (i.e. data that can be put on a map) for our research, art, or story. We need satellite images for disaster
response and navigation. In order to do cool stuff with maps, we need to know how to work with the data.

In order to learn the tools of the trade and get to the good stuff, I created this guide for students or anyone who wants to know how to use spatial data. Read on!
