Projections - Squash an Orange
==============================
Projections go hand in hand with being a data janitor,
because sometimes some of your data is in one projection,
but another part is different. In order to deal with this,
you should be prepared with information about which projection
is standard practice in your field (if any projection at all)
and whether or not you need to be changing projections all the time. 
(see the section on "Scripting - Unleash the Code")
