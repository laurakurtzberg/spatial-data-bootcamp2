What is this Data!?
===================

Spatial Data
------------
Spatial data is data that can be put on a geographic map. In other words, it is information that has an associated geographic location. There are two main types of spatial data: raster and vector.

Raster data
------------
The most recognizable example of raster data is a satellite image. Satellite images are, simply put, pictures of the earth from space. Just like the pictures you can take with a hand-held digital camera, the information is in the pixels. Each pixel has a number value that translates into a color. Raster data is good for investigating what is happening on earth in real time, or for data which is continuous.

Vector data
------------
Vector data is organized differently. Vector data contains features (features are usually points, lines or polygons) which represent things in the real world. For example, a vector data file might include a river represented by a line. Now that whole line can contain information about the river (as opposed to defining that information pixel by pixel). For data which is associated with categorical data or entities like countries, mountains, roads, etc. vector data is the way to go.


Common Data Formats
-------------------
The goal of your project will ultimately define whether you use raster or vector data. Here are some common data formats you may encounter:

Raster:
GeoTIFF: A standard that allows embedding geographic information in a TIFF file.

NetCDF: A set of software libraries and formats that supports data of many variables and multiple dimensions.

Esri grid: Proprietary formats by Esri

Vector:
Shapefile: Stores point, line, and polygon data. This file type is actually made up of a collection of files with the following endings:
    These are mandatory: .shp, .shx, .dbf
There are many others which are optional, including the important .prj file that tells you about the [projection](projections.md) information for that data.

GeoJSON: Encodes points, lines, and polygons (and collections of those) along with information about them using JSON (JavaScript Object Notation). This format is popular for web maps.

Vector geodatabase: There are various databases that, with extensions, can hold geographic objects. Some notable examples are SQLite with the SpatiaLite extension, and PostgreSQL with PostGIS.

There are many more... For other file formats [wikipedia](https://en.wikipedia.org/wiki/GIS_file_formats) is helpful.


Next we'll cover getting some of that data!
