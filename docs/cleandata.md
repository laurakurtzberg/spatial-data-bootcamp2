Be the Data Janitor
===================
We will look at three different methods for cleaning
up our data - doing it in QGIS, with GDAL utilities in the command line, or with R.

Introduction to QGIS
--------------------
QGIS is an excellent open-source desktop Geographic Information System (GIS) software.
When you install QGIS and open the 'Processing Toolbox' by clicking on the 'Processing' menu and then 'Toolbox', you can get an idea of the hundreds of things QGIS can do. QGIS calls these 'geoalgorithms'.

Introduction to GDAL
--------------------
GDAL is an important free software project that allows us to read and write spatial data formats (and do cool stuff to them along the way like write to a different format). You may have noticed a category for GDAL geoalgorithms in the 'Processing Toolbox' in QGIS. GDAL can be used within desktop softwares like QGIS, or with the command line. Its command-line utilities are useful when we are [writing scripts](scripting.md).

Introduction to R
-----------------
R is a programming language and free software that is usually used for statistics and data analysis. The cool thing about R is that there are various libraries (ahem, GDAL) that can turn R into a GIS tool. If you are already comfortable with R, then you will be glad to know that you can make maps with it, too.

Common Cleaning Operations
--------------------------
Some data cleaning operations are most easily done in QGIS, so we'll start with those. Intersect and Union and Join...

Now we will cover things you can do in any of the three.
If you are not experienced in any of them and you like to be able to click on things, use QGIS.
Otherwise, choose the method you like best and read on!

Whether you are using QGIS, GDAL, or R, you can perform operations using GDAL. So you can change projection, change file format, and merge files using any one of the three.
